﻿using System;
using System.Text;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Snakes___The_Evolution_CLI_Client
{

    public class Game_Information
    {
        public static readonly string GAME_NAME = "Snakes - The Evolution";
        public static readonly string GAME_VERSION = typeof(Program).Assembly.GetName().Version.ToString();
        public static readonly string GAME_REPOSITORY = "https://gitlab.com/pwrscript-studios/snakes-the-evolution";
    }

    class Program
    {

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();
        private static IntPtr ThisConsole = GetConsoleWindow();
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        private const int HIDE = 0;
        private const int MAXIMIZE = 3;
        private const int MINIMIZE = 6;
        private const int RESTORE = 9;

        static void Main(string[] args)
        {
            // args
            string GAME_DATA_PATH = Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "PWRScript Studios", "Snakes - The Evolution");

            // Console Configuration
            Console.Title = $"{Game_Information.GAME_NAME} Play Client [CLI]";
            Console.SetWindowSize(Console.LargestWindowWidth, Console.LargestWindowHeight);
            ShowWindow(ThisConsole, MAXIMIZE);
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
            Console.CursorVisible = false;
            Stopwatch game_timer = new Stopwatch();
            int fps = 0,frames_rendered=0;
            Stopwatch total_timer = new Stopwatch();

            Stopwatch tick_timer = new Stopwatch();
            // Variables
            bool is_game_runnable = true;

            Render GAME_RENDER = new Render();

            // Game Mainloop and Logic
            while (is_game_runnable)
            {
                LoadingScreenRender LOADING_SCREEN_RENDER = new LoadingScreenRender
                {
                    LoadingTitle = "Procurando pelos dados..."
                };

                LOADING_SCREEN_RENDER.RenderProgressBar();



                if (Directory.Exists(GAME_DATA_PATH) == false)
                {
                    LOADING_SCREEN_RENDER.LoadingTitle = "Instalando os Dados de Jogo...";
                    Directory.CreateDirectory(GAME_DATA_PATH);
                } else
                {
                    LOADING_SCREEN_RENDER.LoadingTitle = "Carregando os Dados de Jogo...";
                }
                GameFiles.GamePlayerData GAME_PLAYER_DATA = new GameFiles.GamePlayerData(Path.Join(GAME_DATA_PATH,"player_data.dat"));

                for (int i = 1; i <= 10; i++)
                {
                    LOADING_SCREEN_RENDER.SetProgressBar();

                    LOADING_SCREEN_RENDER.LoadingDescription = $"{i}% Instalado";
                    LOADING_SCREEN_RENDER.RenderProgressBar();
                    Thread.Sleep(25);
                }

                SplashScreenRender SPLASH_SCREEN_RENDER = new SplashScreenRender();
                SPLASH_SCREEN_RENDER.RenderScreen(GAME_PLAYER_DATA.PlayerData.Player, GAME_PLAYER_DATA.PlayerData.XP);
                while (is_game_runnable)
                {
                    
                    if (SPLASH_SCREEN_RENDER.ProcessInput() && SPLASH_SCREEN_RENDER.can_stop == false)
                    {
                        
                        SPLASH_SCREEN_RENDER.RenderScreen(GAME_PLAYER_DATA.PlayerData.Player, GAME_PLAYER_DATA.PlayerData.XP);
                        if (SPLASH_SCREEN_RENDER.execute_option)
                        {
                            SPLASH_SCREEN_RENDER.execute_option = false;
                            switch (SPLASH_SCREEN_RENDER.option)
                            {
                                case 0:
                                    GameCreatorScreenRender GAME_CREATOR_SCREEN_RENDER = new GameCreatorScreenRender();
                                    while (GAME_CREATOR_SCREEN_RENDER.cancelled == false)
                                    {
                                        if (GAME_CREATOR_SCREEN_RENDER.ProcessInput())
                                        {
                                            if (GAME_CREATOR_SCREEN_RENDER.execute_option)
                                            {
                                                GAME_CREATOR_SCREEN_RENDER.execute_option = false;
                                                switch (GAME_CREATOR_SCREEN_RENDER.option)
                                                {
                                                    case 2:
                                                        if (GAME_CREATOR_SCREEN_RENDER.action == 0)
                                                        {
                                                            LOADING_SCREEN_RENDER = new LoadingScreenRender
                                                            {
                                                                LoadingTitle = "Iniciando Sessão de Jogo"
                                                            };
                                                            LOADING_SCREEN_RENDER.RenderProgressBar();
                                                            
                                                            GameManager GAME_MANAGER = new GameManager((Types.GAMEMODE)GAME_CREATOR_SCREEN_RENDER.gamemode,Types.ParseGameProperties((Types.GAMEDIFFICULTY)GAME_CREATOR_SCREEN_RENDER.gamedifficulty, (Types.BORDER)GAME_CREATOR_SCREEN_RENDER.border, (Types.TAIL)GAME_CREATOR_SCREEN_RENDER.tail, GAME_CREATOR_SCREEN_RENDER.speed,GAME_CREATOR_SCREEN_RENDER.progressivespeed));

                                                            ClassicSnakeRender CLASSIC_GAME_RENDER = new ClassicSnakeRender(GAME_MANAGER.Game);
                                                            
                                                            for (int i = 1; i <= 100; i++)
                                                            {
                                                                LOADING_SCREEN_RENDER.SetProgressBar();

                                                                LOADING_SCREEN_RENDER.LoadingDescription = $"Gerando Terreno...";
                                                                LOADING_SCREEN_RENDER.RenderProgressBar();
                                                                Thread.Sleep(25);
                                                            }

                                                            CLASSIC_GAME_RENDER.Game.Snakes.Add(new ClassicSnake.Snake() { ForegroundColor = GAME_PLAYER_DATA.PlayerData.PreferedForegroundColor,BackgroundColor = GAME_PLAYER_DATA.PlayerData.PreferedBackgroundColor,Repr= GAME_PLAYER_DATA.PlayerData.PreferedSnakeRepr});
                                                            CLASSIC_GAME_RENDER.Game.GenFood();
                                                            Console.CursorVisible = false;

                                                            game_timer.Start();
                                                            total_timer.Start();

                                                            tick_timer.Start();
                                                            CLASSIC_GAME_RENDER.Game.Tick();
                                                            new Thread(new ThreadStart(CLASSIC_GAME_RENDER.Game.ProcessInputLoop)).Start();
                                                            while (CLASSIC_GAME_RENDER.Game.game_over == false)
                                                            {
                                                                //CLASSIC_GAME_RENDER.Game.ProcessInput();
                                                                

                                                                if (tick_timer.ElapsedMilliseconds >= 1000/CLASSIC_GAME_RENDER.Game.Properties.Speed - CLASSIC_GAME_RENDER.Game.Properties.ProgressiveSpeed)
                                                                {
                                                                    CLASSIC_GAME_RENDER.Game.Tick();

                                                                    CLASSIC_GAME_RENDER.RenderScreen();
                                                                    frames_rendered++;
                                                                    tick_timer.Restart();
                                                                }

                                                                if (total_timer.ElapsedMilliseconds >= 1000)
                                                                {
                                                                    fps = frames_rendered;
                                                                    frames_rendered = 0;
                                                                    Console.Title = fps.ToString();

                                                                }
                                                            }
                                                            total_timer.Stop();

                                                            Console.SetCursorPosition(0, 0);
                                                            Console.WriteLine("╔════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗"); //0 
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                        Perdeu                                                                  ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║     Tempo:                                                                                                     ║");
                                                            Console.WriteLine("║     Score Final:                                                                                               ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                                                                                                                ║");
                                                            Console.WriteLine("║                              Pressione ESC para sair ou Enter para Reiniciar o Jogo...                         ║");
                                                            Console.WriteLine("╚════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝"); //22
                                                            Console.SetCursorPosition(13, 11);
                                                            Console.Write(total_timer.Elapsed.ToString());
                                                            Console.SetCursorPosition(19, 12);

                                                            Console.Write(CLASSIC_GAME_RENDER.Game.Snakes[0].score);
                                                            while (Console.ReadKey(true).Key != ConsoleKey.Enter)
                                                            {

                                                            }

                                                        } else
                                                        {
                                                            GAME_CREATOR_SCREEN_RENDER.cancelled = true;
                                                        }
                                                        break;
                                                }
                                            } else
                                            {

                                                GAME_CREATOR_SCREEN_RENDER.RenderScreen();
                                            }
                                        }
                                    }
                                    SPLASH_SCREEN_RENDER.RenderScreen(GAME_PLAYER_DATA.PlayerData.Player,GAME_PLAYER_DATA.PlayerData.XP);
                                    break;
                                case 4:
                                    Environment.Exit(0);
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else if (SPLASH_SCREEN_RENDER.can_stop == true)
                    {

                        is_game_runnable = false;
                    }
                }
            }
        }
    }
}

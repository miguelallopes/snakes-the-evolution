﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snakes___The_Evolution_CLI_Client
{
    public class Render
    {

        public void ClearScreen()
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(@"                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  
                                                                                                                  ");
            Console.SetCursorPosition(0, 0);
        }
        public void RenderBorder()
        {
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(@"╔════════════════════════════════════════════════════════════════════════════════════════════════════════════════╗
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
║                                                                                                                ║
╚════════════════════════════════════════════════════════════════════════════════════════════════════════════════╝");

            Console.SetCursorPosition(0, 0);
        }
        public Render()
        {
            Console.InputEncoding = Encoding.UTF8;
            Console.OutputEncoding = Encoding.UTF8;
        }
    }

    public class LoadingScreenRender : Render
    {
        private bool use_del = false;
        private int state = 1;

        public string LoadingTitle { get; set; }
        public string LoadingDescription { get; set; }

        public void RenderScreen()
        {
            // ANSI Shadow
            string logo = @"        ██████╗ ██╗    ██╗██████╗ ███████╗ ██████╗██████╗ ██╗██████╗ ████████╗    
║                      ██╔══██╗██║    ██║██╔══██╗██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝    
║                      ██████╔╝██║ █╗ ██║██████╔╝███████╗██║     ██████╔╝██║██████╔╝   ██║       
║                      ██╔═══╝ ██║███╗██║██╔══██╗╚════██║██║     ██╔══██╗██║██╔═══╝    ██║       
║                      ██║     ╚███╔███╔╝██║  ██║███████║╚██████╗██║  ██║██║██║        ██║       
║                      ╚═╝      ╚══╝╚══╝ ╚═╝  ╚═╝╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝        ╚═╝
║
║                              ███████╗████████╗██╗   ██╗██████╗ ██╗ ██████╗ ███████╗                
║                              ██╔════╝╚══██╔══╝██║   ██║██╔══██╗██║██╔═══██╗██╔════╝                
║                              ███████╗   ██║   ██║   ██║██║  ██║██║██║   ██║███████╗                
║                              ╚════██║   ██║   ██║   ██║██║  ██║██║██║   ██║╚════██║                
║                              ███████║   ██║   ╚██████╔╝██████╔╝██║╚██████╔╝███████║                
║                              ╚══════╝   ╚═╝    ╚═════╝ ╚═════╝ ╚═╝ ╚═════╝ ╚══════╝";

            RenderBorder();
            Console.SetCursorPosition(15, 4);
            Console.Write(logo);

            Console.SetCursorPosition(2, 19);
            Console.Write($"{Game_Information.GAME_NAME}, v{Game_Information.GAME_VERSION}");
            Console.SetCursorPosition(100, 19);
            Console.Write($"by PWRScript");
            RenderProgressBar();

            Console.SetCursorPosition(0, 0);
        }



        public void RenderProgressBar()
        {
            if (use_del)
            {
                switch (state)
                {
                    case 1:

                        Console.SetCursorPosition(55, 23);
                        Console.Write("   ");
                        break;
                    case 2:

                        Console.SetCursorPosition(58, 23);
                        Console.Write(" ");
                        break;
                    case 3:

                        Console.SetCursorPosition(58, 22);
                        Console.Write(" ");
                        break;
                    case 4:

                        Console.SetCursorPosition(58, 21);
                        Console.Write(" ");
                        break;
                    case 5:

                        Console.SetCursorPosition(55, 21);
                        Console.Write("   ");
                        break;
                    case 6:

                        Console.SetCursorPosition(54, 21);
                        Console.Write(" ");
                        break;
                    case 7:

                        Console.SetCursorPosition(54, 22);
                        Console.Write(" ");
                        break;

                    case 8:

                        Console.SetCursorPosition(54, 23);
                        Console.Write(" ");
                        use_del = false;
                        break;
                    default:
                        break;
                }
            }
            else
            {

                switch (state)
                {
                    case 1:

                        Console.SetCursorPosition(55, 23);
                        Console.Write("═══");
                        break;
                    case 2:

                        Console.SetCursorPosition(58, 23);
                        Console.Write("╝");
                        break;
                    case 3:

                        Console.SetCursorPosition(58, 22);
                        Console.Write("║");
                        break;
                    case 4:

                        Console.SetCursorPosition(58, 21);
                        Console.Write("╗");
                        break;
                    case 5:

                        Console.SetCursorPosition(55, 21);
                        Console.Write("═══");
                        break;
                    case 6:

                        Console.SetCursorPosition(54, 21);
                        Console.Write("╔");
                        break;
                    case 7:

                        Console.SetCursorPosition(54, 22);
                        Console.Write("║");
                        break;

                    case 8:

                        Console.SetCursorPosition(54, 23);
                        Console.Write("╚");
                        use_del = true;
                        break;
                    default:
                        break;
                }
            }
            if (LoadingTitle.Length != 0)
            {


                Console.SetCursorPosition((115 / 2) - (LoadingTitle.Length / 2), 25);
                Console.Write(LoadingTitle);
            }
            if (LoadingDescription.Length != 0)
            {

                Console.SetCursorPosition((115 / 2) - (LoadingDescription.Length / 2), 26);
                Console.Write(LoadingDescription);
            }
            Console.SetCursorPosition(0, 0);
        }

        public void SetProgressBar(int state_to_def = 9)
        {
            if (state_to_def == 9)
            {
                if (state == 8)
                {
                    state = 1;
                }
                else
                {
                    state++;
                }
            }
            else if (state_to_def >= 10)
            {
                if (state == 1)
                {
                    state = 8;
                }
                else
                {
                    state--;
                }
            }
            else
            {
                state = state_to_def;
            }
        }



        public LoadingScreenRender()
        {
            LoadingTitle = "";

            LoadingDescription = "";
            ClearScreen();
            RenderScreen();
        }
    }

    public class GameCreatorScreenRender : Render
    {

        public bool cancelled = false;


        public int gamemode = 0;

        public int gamedifficulty = 1;
        public int border = 0;

        public int tail = 0;
        public double speed = 12;
        public double progressivespeed = 1.0;
        public int action = 0;

        public bool personalizing = false;
        public int personalizing_select = 0; 

        public bool execute_option = false;
        public int option = 0;

        
        public bool ProcessInput()
        {
            if (Console.KeyAvailable)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.UpArrow:
                        if (option == 1 && personalizing == true)
                        {
                            if (personalizing_select > 0)
                            {

                                personalizing_select--;
                            }

                        }
                        else if (option > 0)
                        {
                            option--;
                        }
                        else
                        {
                            option = 2;
                        }
                        return true;
                        break;
                    case ConsoleKey.DownArrow:
                        if (option == 1 && personalizing == true)
                        {
                            if (personalizing_select < 3)
                            {

                                personalizing_select++;
                            }

                        }
                        
                        else if (option < 2)
                        {
                            option++;
                        }
                        else
                        {
                            option = 0;
                        }
                        return true;
                        break;
                    case ConsoleKey.LeftArrow:
                        switch (option)
                        {
                            case 0:
                                if (gamemode > 0)
                                {

                                    gamemode--;
                                }
                                break;
                            case 1:
                                if (gamedifficulty > 0 && personalizing == false)
                                {

                                    gamedifficulty--;
                                }
                                else if (personalizing == true)
                                {
                                    switch (personalizing_select)
                                    {
                                        case 0:
                                            if (border > 0)
                                            {

                                                border--;
                                            }
                                            break;
                                        case 1:
                                            if (tail > 0)
                                            {

                                                tail--;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                if (action > 0)
                                {

                                    action--;
                                }
                                break;
                            default:
                                break;
                        }
                        return true;
                        break;
                    case ConsoleKey.RightArrow:
                        switch (option)
                        {
                            case 0:
                                if (gamemode < Enum.GetNames(typeof(Types.GAMEMODE)).Length - 1)
                                {

                                    gamemode++;
                                }
                                break;
                            case 1:
                                if (gamedifficulty < Enum.GetNames(typeof(Types.GAMEDIFFICULTY)).Length - 1 && personalizing == false)
                                {

                                    gamedifficulty++;
                                } else if (personalizing == true)
                                {
                                    switch (personalizing_select)
                                    {
                                        case 0:
                                            if (border < Enum.GetNames(typeof(Types.BORDER)).Length - 1)
                                            {

                                                border++;
                                            }
                                            break;
                                        case 1:
                                            if (tail < Enum.GetNames(typeof(Types.TAIL)).Length - 1)
                                            {

                                                tail++;
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                                break;
                            case 2:
                                if (action < 1)
                                {

                                    action++;
                                }
                                break;
                            default:
                                break;
                        }
                        return true;
                        break;
                    case ConsoleKey.Enter:
                        if (option == 2)
                        {

                            execute_option = true;
                        } else if (option == 1 && (Types.GAMEDIFFICULTY)gamedifficulty == Types.GAMEDIFFICULTY.PERSONALIZED)
                        {
                            personalizing = true;
                        }
                        return true;
                        break;
                    case ConsoleKey.Escape:
                        if (option == 1 && personalizing == true)
                        {

                            personalizing = false;
                            RenderScreen();

                            
                        }
                        else 
                        {
                            cancelled = true;
                        }
                        break;
                    default:
                        break;
                }

                return false;
            }
            else
            {
                return false;
            }
        }
        public void RenderScreen()
        {
            string logo = @"        ██████╗ ██╗    ██╗██████╗ ███████╗ ██████╗██████╗ ██╗██████╗ ████████╗    
║                      ██╔══██╗██║    ██║██╔══██╗██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝    
║                      ██████╔╝██║ █╗ ██║██████╔╝███████╗██║     ██████╔╝██║██████╔╝   ██║       
║                      ██╔═══╝ ██║███╗██║██╔══██╗╚════██║██║     ██╔══██╗██║██╔═══╝    ██║       
║                      ██║     ╚███╔███╔╝██║  ██║███████║╚██████╗██║  ██║██║██║        ██║       
║                      ╚═╝      ╚══╝╚══╝ ╚═╝  ╚═╝╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝        ╚═╝
║
║                              ███████╗████████╗██╗   ██╗██████╗ ██╗ ██████╗ ███████╗                
║                              ██╔════╝╚══██╔══╝██║   ██║██╔══██╗██║██╔═══██╗██╔════╝                
║                              ███████╗   ██║   ██║   ██║██║  ██║██║██║   ██║███████╗                
║                              ╚════██║   ██║   ██║   ██║██║  ██║██║██║   ██║╚════██║                
║                              ███████║   ██║   ╚██████╔╝██████╔╝██║╚██████╔╝███████║                
║                              ╚══════╝   ╚═╝    ╚═════╝ ╚═════╝ ╚═╝ ╚═════╝ ╚══════╝";

            RenderBorder();
            Console.SetCursorPosition(15, 4);
            Console.Write(logo);
            Console.SetCursorPosition(2, 19);
            Console.Write($"{Game_Information.GAME_NAME}, v{Game_Information.GAME_VERSION}");
            Console.SetCursorPosition(100, 19);
            Console.Write($"by PWRScript");

            

            switch (option)
            {
                case 0:

                    Console.SetCursorPosition(10, 21);
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write("Tipo de Jogo: ");
                    switch (gamemode)
                    {
                        case 0:
                            Console.Write("Clássico");
                            break;
                        default:
                            break;
                    }


                    Console.ResetColor();
                    Console.SetCursorPosition(10, 22);

                    Console.Write("Dificuldade: ");
                    switch (gamedifficulty)
                    {
                        case 0:
                            Console.Write("Fácil");
                            break;
                        case 1:
                            Console.Write("Médio");
                            break;
                        case 2:
                            Console.Write("Dificil");
                            break;

                        case 3:
                            Console.Write("Personalizada");
                            Console.ResetColor();
                            Console.SetCursorPosition(50, 21);
                            Console.Write($"Tipo de Borda: [{(Types.BORDER)border}]");
                            Console.SetCursorPosition(50, 22);

                            Console.Write($"Colisão Cauda: [{(Types.TAIL)tail}]");
                            break;
                        default:
                            break;
                    }


                    Console.ResetColor();
                    
                        
                            

                    Console.SetCursorPosition(10, 24);
                    Console.Write("Iniciar");
                    Console.Write("             Cancelar");
                            
                    
                    break;
                case 1:
                    Console.SetCursorPosition(10, 21);
                    Console.Write("Tipo de Jogo: ");
                    switch (gamemode)
                    {
                        case 0:
                            Console.Write("Clássico");
                            break;
                        default:
                            break;
                    }

                    Console.SetCursorPosition(10, 22);
                    if (personalizing == false)
                    {
                       

                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write("Dificuldade: ");
                        switch (gamedifficulty)
                        {
                            case 0:
                                Console.Write("Fácil");
                                break;
                            case 1:
                                Console.Write("Médio");
                                break;
                            case 2:
                                Console.Write("Dificil");
                                break;

                            case 3:
                                Console.Write("Personalizada");

                                Console.ResetColor();
                                Console.SetCursorPosition(50, 21);
                                Console.Write($"Tipo de Borda: [{(Types.BORDER)border}]");
                                Console.SetCursorPosition(50, 22);

                                Console.Write($"Colisão Cauda: [{(Types.TAIL)tail}]");
                                break;
                            default:
                                break;
                        }


                        Console.ResetColor();


                    } else
                    {
                        Console.Write("Dificuldade: ");
                        switch (gamedifficulty)
                        {
                            case 0:
                                Console.Write("Fácil");
                                break;
                            case 1:
                                Console.Write("Médio");
                                break;
                            case 2:
                                Console.Write("Dificil");
                                break;

                            case 3:
                                Console.Write("Personalizada");

                                Console.ResetColor();
                                switch (personalizing_select)
                                {
                                    case 0:

                                        Console.BackgroundColor = ConsoleColor.White;
                                        Console.ForegroundColor = ConsoleColor.Black;
                                        Console.SetCursorPosition(50, 21);
                                        Console.Write($"Tipo de Borda: [{(Types.BORDER)border}]");

                                        Console.ResetColor();
                                        Console.SetCursorPosition(50, 22);

                                        Console.Write($"Colisão Cauda: [{(Types.TAIL)tail}]");
                                        break;
                                    case 1:
                                        Console.SetCursorPosition(50, 21);
                                        Console.Write($"Tipo de Borda: [{(Types.BORDER)border}]");

                                        Console.BackgroundColor = ConsoleColor.White;
                                        Console.ForegroundColor = ConsoleColor.Black;
                                        Console.SetCursorPosition(50, 22);

                                        Console.Write($"Colisão Cauda: [{(Types.TAIL)tail}]");

                                        Console.ResetColor();
                                        break;
                                    default:
                                        break;
                                }
                                
                                break;
                            default:
                                break;
                        }
                    }
                    Console.SetCursorPosition(10, 24);
                    Console.Write("Iniciar");
                    Console.Write("             Cancelar");
                    break;
                case 2:

                    Console.SetCursorPosition(10, 21);
                    Console.Write("Tipo de Jogo: ");
                    switch (gamemode)
                    {
                        case 0:
                            Console.Write("Clássico");
                            break;
                        default:
                            break;
                    }

                    Console.SetCursorPosition(10, 22);

                    
                    Console.Write("Dificuldade: ");
                    switch (gamedifficulty)
                    {
                        case 0:
                            Console.Write("Fácil");
                            break;
                        case 1:
                            Console.Write("Médio");
                            break;
                        case 2:
                            Console.Write("Dificil");
                            break;

                        case 3:
                            Console.Write("Personalizada");
                            Console.SetCursorPosition(50, 21);
                            Console.Write($"Tipo de Borda: [{(Types.BORDER)border}]");
                            Console.SetCursorPosition(50, 22);

                            Console.Write($"Colisão Cauda: [{(Types.TAIL)tail}]");
                            break;
                        default:
                            break;
                    }
                    switch (action)
                    {
                        case 0:
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;

                            Console.SetCursorPosition(10, 24);
                            Console.Write("Iniciar");
                            Console.ResetColor();
                            Console.Write("             Cancelar");
                            break;
                        case 1:

                            Console.SetCursorPosition(10, 24);
                            Console.Write("Iniciar             ");

                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.Black;
                            Console.Write("Cancelar");
                            Console.ResetColor();
                            break;
                        default:
                            break;
                    }
                    
                    break;
                default:
                    break;
            }

            


        }
        public GameCreatorScreenRender()
        {
            ClearScreen();
            RenderScreen();

        }

    }
    public class SplashScreenRender : Render
    {
        public int option = 0;
        public bool can_stop = false;

        public bool execute_option = false;
        public void RenderScreen(string player, int xp)
        {
            string logo = @"        ██████╗ ██╗    ██╗██████╗ ███████╗ ██████╗██████╗ ██╗██████╗ ████████╗    
║                      ██╔══██╗██║    ██║██╔══██╗██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝    
║                      ██████╔╝██║ █╗ ██║██████╔╝███████╗██║     ██████╔╝██║██████╔╝   ██║       
║                      ██╔═══╝ ██║███╗██║██╔══██╗╚════██║██║     ██╔══██╗██║██╔═══╝    ██║       
║                      ██║     ╚███╔███╔╝██║  ██║███████║╚██████╗██║  ██║██║██║        ██║       
║                      ╚═╝      ╚══╝╚══╝ ╚═╝  ╚═╝╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝        ╚═╝
║
║                              ███████╗████████╗██╗   ██╗██████╗ ██╗ ██████╗ ███████╗                
║                              ██╔════╝╚══██╔══╝██║   ██║██╔══██╗██║██╔═══██╗██╔════╝                
║                              ███████╗   ██║   ██║   ██║██║  ██║██║██║   ██║███████╗                
║                              ╚════██║   ██║   ██║   ██║██║  ██║██║██║   ██║╚════██║                
║                              ███████║   ██║   ╚██████╔╝██████╔╝██║╚██████╔╝███████║                
║                              ╚══════╝   ╚═╝    ╚═════╝ ╚═════╝ ╚═╝ ╚═════╝ ╚══════╝";

            RenderBorder();
            Console.SetCursorPosition(15, 4);
            Console.Write(logo);
            Console.SetCursorPosition(2, 19);
            Console.Write($"{Game_Information.GAME_NAME}, v{Game_Information.GAME_VERSION}");
            Console.SetCursorPosition(100, 19);
            Console.Write($"by PWRScript");

            Console.SetCursorPosition(80, 21);
            Console.Write("╔═P═E═R═F╦I═L═══════════════╗");
            Console.SetCursorPosition(80, 22);
            Console.Write("║ Player ║                  ║");

            Console.SetCursorPosition(91, 22);
            Console.Write(player);
            Console.SetCursorPosition(80, 23);

            Console.Write("╠════════╬══════════════════╣");
            Console.SetCursorPosition(80, 24);
            int level = (xp / 10) + 1;
            int rest_xp = (xp % 10);

            if (level > 1000)
            {

                Console.Write("║ Nível  ║ 999 [#########]  ║");
            } else
            {
                
                Console.Write("║ Nível  ║     [         ]  ║");
                Console.SetCursorPosition(90 + (4- level.ToString().Length), 24);
                Console.Write(level);
                if (rest_xp != 0)
                {
                    string rend_niv = "";
                    for (int i = 0; i < rest_xp; i++)
                    {
                        rend_niv += "#";
                    }

                    Console.SetCursorPosition(96, 24);
                    Console.Write(rend_niv);
                }
            }

            Console.SetCursorPosition(80, 25);
            Console.Write("╚════════╩══════════════════╝");

            Console.SetCursorPosition(10, 21);
            Console.Write("[ ] Criar Jogo");
            Console.SetCursorPosition(10, 22);
            Console.Write("[ ] Juntar-se a Sessão Multiplayer");
            Console.SetCursorPosition(10, 23);
            Console.Write("[ ] Estatisticas e Troféus");
            Console.SetCursorPosition(10, 24);
            Console.Write("[ ] Definições");
            Console.SetCursorPosition(10, 25);
            Console.Write("[ ] Sair");
            Console.SetCursorPosition(11, 21 + option);
            Console.Write(">");
        }

        public bool ProcessInput()
        {
            if (Console.KeyAvailable)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.UpArrow:
                        if (option > 0)
                        {
                            option--;
                        }
                        else
                        {
                            option = 4;
                        }
                        return true;
                        break;
                    case ConsoleKey.DownArrow:
                        if (option < 4)
                        {
                            option++;
                        }
                        else
                        {
                            option = 0;
                        }
                        return true;
                        break;
                    case ConsoleKey.Enter:
                        execute_option = true;
                        return true;
                        break;
                    case ConsoleKey.Escape:


                        can_stop = true;
                        break;
                    default:
                        break;
                }

                return false;
            } else
            {
                return false;
            }
        }

        public SplashScreenRender()
        {
            ClearScreen();

        }
    }

    public class ClassicSnakeRender : Render
    {
        public GameManager.ClassicGameManager Game { get; set; }

        public void RenderScreen()
        {
            RenderBorder();

            foreach (ClassicSnake.Snake snake in Game.Snakes)
            {
                Console.BackgroundColor = snake.BackgroundColor;
                Console.ForegroundColor = snake.ForegroundColor;
                Console.SetCursorPosition(snake.x + 1, snake.y + 1);
                Console.Write("#");
                ClassicSnake.Last_Coord coord;
                for (int i = 1; i <= snake.score; i++)
                {
                    
                        
                        coord = snake.last_coords[snake.last_coords.Count - i];
                        Console.SetCursorPosition(coord.x + 1, coord.y + 1);
                        Console.Write(snake.Repr);

                }
                Console.ResetColor();
            }

            foreach (ClassicSnake.Food food in Game.Foods)
            {
                Console.SetCursorPosition(food.x + 1, food.y + 1);
                Console.Write("J");
            }
        }

        public ClassicSnakeRender(GameManager.ClassicGameManager game)
        {
            Game = game;
            ClearScreen();
            RenderScreen();
        }
    }
}

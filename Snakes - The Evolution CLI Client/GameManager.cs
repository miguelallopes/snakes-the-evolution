﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snakes___The_Evolution_CLI_Client
{
    public class ClassicSnake
    {

        public enum Direction
        {
            UP,
            DOWN,
            LEFT,
            RIGHT,
            STOPPED,
        }

        public class Last_Coord
        {
            public int x;
            public int y;

            public Last_Coord(int X, int Y)
            {
                x = X;
                y = Y;
            }
        }
        public class Snake
        {
            


            public List<Last_Coord> last_coords;
            public int x = 0;
            public int y = 0;
            public int last_x = 0;
            public int last_y = 0;
            public int score = 1;

            public ConsoleColor ForegroundColor = ConsoleColor.White;
            public ConsoleColor BackgroundColor = ConsoleColor.Black;
            public char Repr = '#';
            public Direction direction = Direction.STOPPED;

            public bool Move(Direction direction_to_move, Types.GAMEPROPERTIES Properties)
            {
                Last_Coord coord;



                switch (direction_to_move)
                {
                    case Direction.UP:
                        switch (Properties.Border)
                        {
                            case Types.BORDER.STOP:
                                if (y - 1 >= 0)
                                {

                                    y--;
                                    return true;
                                }
                                else
                                {
                                    direction = Direction.STOPPED;
                                    return true;
                                }
                                break;
                            case Types.BORDER.BYPASS:
                                if (y - 1 >= 0)
                                {

                                    y--;
                                    return true;
                                }
                                else
                                {
                                    y = 26;
                                    return true;
                                }
                                break;
                            case Types.BORDER.KILL:
                                if (y - 1 >= 0)
                                {

                                    y--;
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                                break;
                            default:
                                return true;
                                break;
                        }

                        
                        break;
                    case Direction.DOWN:
                        switch (Properties.Border)
                        {
                            case Types.BORDER.STOP:
                                if (y - 1 <= 24)
                                {

                                    y++;
                                    return true;
                                }
                                else
                                {
                                    direction = Direction.STOPPED;
                                    return true;
                                }
                                break;
                            case Types.BORDER.BYPASS:
                                if (y + 1 <= 26)
                                {

                                    y++;
                                    return true;
                                }
                                else
                                {
                                    y = 0;
                                    return true;
                                }
                                break;
                            case Types.BORDER.KILL:
                                if (y + 1 <= 26)
                                {

                                    y++;
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            default:
                                return true;
                                break;
                        }
                        
                        break;
                    case Direction.LEFT:
                        switch (Properties.Border)
                        {
                            case Types.BORDER.STOP:
                                if (x - 1 >= 0)
                                {

                                    x--;
                                    return true;
                                }
                                else
                                {
                                    direction = Direction.STOPPED;
                                    return true;
                                }
                                break;
                            case Types.BORDER.BYPASS:
                                if (x - 1 >= 0)
                                {

                                    x--;
                                    return true;
                                }
                                else
                                {
                                    x = 111;
                                    return true;
                                }
                                break;
                            case Types.BORDER.KILL:
                                if (x - 1 >= 0)
                                {

                                    x--;
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                                break;
                            default:
                                return true;
                                break;
                        }

                        
                        break;
                    case Direction.RIGHT:
                        switch (Properties.Border)
                        {
                            case Types.BORDER.STOP:
                                if (x + 1 <= 111)
                                {

                                    x++;
                                    return true;
                                }
                                else
                                {
                                    direction = Direction.STOPPED;
                                    return true;
                                }
                                break;
                            case Types.BORDER.BYPASS:
                                if (x + 1 <= 111)
                                {

                                    x++;
                                    return true;
                                }
                                else
                                {
                                    x = 0;
                                    return true;
                                }
                                break;
                            case Types.BORDER.KILL:
                                if (x + 1 <= 111)
                                {

                                    x++;
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                                break;
                            default:
                                return true;
                                break;
                        }

                        break;
                    case Direction.STOPPED:
                        return true;
                        break;
                }
                return false;
            }

            public Snake()
            {
                last_coords = new List<Last_Coord>() { new Last_Coord(x, y) };
            }

        }

        public class Food
        {
            public int x { get; set; }
            public int y { get; set; }

            int regen_quantity { get; set; }

            public Food(int REGEN_QUANTITY = 1, int X = 0, int Y = 0)
            {

                regen_quantity = REGEN_QUANTITY;
                x = X;
                y = Y;
            }

        }
    }

    public class GameManager
    {
        
        public ClassicGameManager Game { get; set; }

        public class ClassicGameManager
        {


            public List<ClassicSnake.Snake> Snakes = new List<ClassicSnake.Snake>();

            public List<ClassicSnake.Food> Foods = new List<ClassicSnake.Food>();
            public Types.GAMEPROPERTIES Properties { get; set; }
            public bool game_over = false;

            public void ProcessInputLoop()
            {
                while (game_over == false)
                {
                    ProcessInput();
                }
            }

            public void ProcessInput()
            {
                if (Console.KeyAvailable)
                {

                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.UpArrow:
                            if (Snakes[0].direction != ClassicSnake.Direction.DOWN)
                            {

                                Snakes[0].direction = ClassicSnake.Direction.UP;
                            }
                            break;
                        case ConsoleKey.DownArrow:
                            if (Snakes[0].direction != ClassicSnake.Direction.UP)
                            {

                                Snakes[0].direction = ClassicSnake.Direction.DOWN;
                            }
                            break;
                        case ConsoleKey.LeftArrow:
                            if (Snakes[0].direction != ClassicSnake.Direction.RIGHT)
                            {
                                Snakes[0].direction = ClassicSnake.Direction.LEFT;
                            }
                            break;
                        case ConsoleKey.RightArrow:
                            if (Snakes[0].direction != ClassicSnake.Direction.LEFT)
                            {
                                Snakes[0].direction = ClassicSnake.Direction.RIGHT;
                            }
                            break;
                        default:
                            break;
                    }
                }

            }

            public int GetFoodIndex(int x, int y)
            {
                int cont = 0;
                foreach (ClassicSnake.Food food in Foods)
                {
                    if (food.x == x && food.y == y)
                    {
                        return cont;
                    }
                    cont++;
                }
                return -1;
            }

            public ClassicSnake.Food GenFood()
            {
                int x = new Random().Next(0, 111);
                int y = new Random().Next(0, 26);
                if (GetFoodIndex(x, y) == -1)
                {
                    ClassicSnake.Food food_to = new ClassicSnake.Food() { x = x, y = y };
                    Foods.Add(food_to);
                    return food_to;
                }
                else
                {
                    return GenFood();
                }
            }

            public void Tick()
            {
                bool moved, lost = false;
               

                foreach (ClassicSnake.Snake snake in Snakes)
                {

                    snake.last_x = snake.x;

                    snake.last_y = snake.y;
                    moved = snake.Move(snake.direction,Properties);

                    switch (Properties.Tail)
                    {
                        case Types.TAIL.BYPASS:

                            break;
                        case Types.TAIL.KILL:
                            for (int i = 1; i <= snake.score; i++)
                            {
                                if (i != 1)
                                {
                                    ClassicSnake.Last_Coord coord = snake.last_coords[snake.last_coords.Count - i];

                                    if (coord.x == snake.x && coord.y == snake.y)
                                    {
                                        lost = true;
                                    }
                                }


                            }
                            break;
                        default:
                            break;
                    }




                    if (moved && lost == false)
                    {
                        int food = GetFoodIndex(snake.x, snake.y);
                        if (food > -1)
                        {
                            Foods.RemoveAt(food);
                            snake.score++;
                            GenFood();
                        }
                        if (snake.last_x != snake.x ^ snake.last_y != snake.y)
                        {
                            snake.last_coords.Add(new ClassicSnake.Last_Coord(snake.x, snake.y));

                        }

                    }
                    else
                    {
                        game_over = true;
                    }

                }

            }

            public ClassicGameManager(Types.GAMEPROPERTIES GameProperties)
            {
                Properties = GameProperties;
            }

        }

        public GameManager(Types.GAMEMODE GameMode, Types.GAMEPROPERTIES GameProperties)
        {
            switch (GameMode)
            {
                case Types.GAMEMODE.CLASSIC:

                    Game = new ClassicGameManager(GameProperties);
                    break;
                default:
                    break;
            }
        }
    }
}

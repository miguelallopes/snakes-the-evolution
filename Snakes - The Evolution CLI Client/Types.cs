﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snakes___The_Evolution_CLI_Client
{
    public class Types
    {

        public enum GAMEMODE
        {
            CLASSIC,
        }

        

        public enum TROPHIES
        {
            FIRST_BIG_MAC, // COLLECT YOUR FIRST FOOD
            GREAT_MEAL, // COLLECT 175 FOODS IN A MEDIUM OR HIGHER DIFFICULTY GAME
            TRYHARDER, // COLLECT 375 FOODS IN A MEDIUM OR HIGHER DIFFICULTY GAME
        }

        public enum GAMEDIFFICULTY
        {
            EASY,
            MEDIUM,
            HARD,
            PERSONALIZED,
        }

        public enum BORDER
        {
            STOP,
            BYPASS,
            KILL,
        }

        public enum TAIL
        {
            BYPASS,
            KILL,
        }

        public class Trophie_Data
        {
            TROPHIES Trophie { get; set; }

            DateTime Unlocked_At { get; set; }
            
        }

        public class Statistics_Data
        {
            public List<int> Games_Scores { get; set; }

            public int Total_Score { get; set; }
            public int Games_Played { get; set; }
            public int Games_Lost { get; set; }

        }

        public static GAMEPROPERTIES ParseGameProperties(GAMEDIFFICULTY Difficulty = GAMEDIFFICULTY.MEDIUM, BORDER Border = BORDER.BYPASS,TAIL Tail = TAIL.KILL,double Speed=12, double ProgressiveSpeed=1.0)
        {
            switch (Difficulty)
            {
                case GAMEDIFFICULTY.EASY:
                    return new GAMEPROPERTIES() {Border = BORDER.STOP,Tail = TAIL.BYPASS,Speed = 10, ProgressiveSpeed = 0.7};
                    break;
                case GAMEDIFFICULTY.MEDIUM:
                    return new GAMEPROPERTIES() { Border = BORDER.BYPASS, Tail = TAIL.KILL, Speed = 12, ProgressiveSpeed = 1.0 };
                    break;
                case GAMEDIFFICULTY.HARD:
                    return new GAMEPROPERTIES() { Border = BORDER.KILL, Tail = TAIL.KILL, Speed = 16, ProgressiveSpeed = 1.5 };
                    break;
                default:

                    return new GAMEPROPERTIES() { Border = Border, Tail = Tail, Speed = Speed,ProgressiveSpeed=ProgressiveSpeed };
                    break;
            }
        }

        public class GAMEPROPERTIES
        {
            public BORDER Border { get; set; }
            public TAIL Tail { get; set; }

            // Quadrados por segundo
            public double Speed { get; set; }

            public double ProgressiveSpeed { get; set; }
        }
    }
}
